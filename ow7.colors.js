// ==UserScript==
// @name          OW7 Color
// @namespace     https://github.com/klebercode/ow7-color
// @version       0.1
// @require       http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js
// @description   Changes background color of Trello cards to match clients
// @match         https://trello.com/b/*
// @copyright     2018+, Kleber Code
// ==/UserScript==


var colorize = function() {
  jQuery(".list-card").each(function (i,o){
    jQuery(o).css("background-color", "").css("border", "");
    $label = jQuery(o).find('.card-label:last');
    $title = jQuery(o).find('.list-card-title');
    $details = jQuery(o).find('.list-card-details');

    a = [
      'GO EAT',
      'OURO VERDE',
      'OW7',
      'PUBLLIQUE',
      'PROTOCOLLE',
      'DELIVERY'
    ];
    c = [
      '#d81b60',  // GO EAT
      '#4caf50',  // OURO VERDE
      '#3f51b5',  // OW7
      '#7e57c2',  // PUBLIIQUE
      '#42a5f5',  // PROTOCOLLE
      '#ff6f00'   // DELIVERY
    ];

    for ( var i = 0; i < a.length; i++ ){
      if ( $title.text().indexOf(a[i]) > -1 ) {
        $(o).css("background-color", c[i]);
        $(o).find('.list-card-title, .badge-icon, .badge-text').css("color", 'white');

        if ( $label.hasClass('card-label-black') ) {
          $(o).css("background-color", "#000");
        }

        break;
      }
    }
  });
};

$(document).ready(function() {
  colorize();
  setInterval(colorize, 1000);
});
